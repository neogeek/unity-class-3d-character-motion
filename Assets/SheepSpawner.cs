﻿using UnityEngine;

public class SheepSpawner : MonoBehaviour {

    public GameObject sheep;

    void Update() {

        Instantiate(sheep, gameObject.transform.position, Quaternion.identity);

    }

}
