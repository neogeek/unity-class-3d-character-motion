﻿using UnityEngine;

public class GuyController : MonoBehaviour
{

    public LayerMask groundLayerMask;

    private float h = 0;
    private float v = 0;

    private Animator animator;
    private Rigidbody rb;

    private readonly float camRayLength = 100f;

    void Awake()
    {

        animator = gameObject.GetComponent<Animator>();
        rb = gameObject.GetComponent<Rigidbody>();

    }

    void Update()
    {

        h = Input.GetAxis("Horizontal");
        v = Input.GetAxis("Vertical");

        if (Input.GetButton("Fire3") && v >= 0.9f)
        {

            v = 2;

        }

        animator.SetFloat("h", h);
        animator.SetFloat("v", v);

    }

    void FixedUpdate()
    {

        if (!Input.GetMouseButton(1))
        {

            Turning();

        }

    }

    void Turning()
    {

        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;

        if (Physics.Raycast(camRay, out hit, camRayLength, groundLayerMask))
        {

            Vector3 playerToMouse = hit.point - gameObject.transform.position;

            playerToMouse.y = 0f;

            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);

            rb.MoveRotation(newRotation);

        }

    }

}
