﻿using UnityEngine;

public class CameraController : MonoBehaviour
{

    public Transform target;

    private readonly float offsetDistance = 2f;
    private readonly float offsetHeight = 0;
    private readonly float rotateSpeedX = 250.0f;
    private readonly float rotateSpeedY = 120.0f;
    private readonly float rotationDampingX = 3f;
    private readonly float rotationDampingy = 2f;

    private float x;
    private float y;

    void Start()
    {

        gameObject.transform.position = new Vector3(
            target.position.x,
            target.position.y + offsetHeight,
            target.position.z - offsetDistance
        );

        gameObject.transform.LookAt(target);

    }

    void LateUpdate()
    {

        if (Input.GetMouseButton(1))
        {

            x += Input.GetAxis("Mouse X") * rotateSpeedX * 0.02f;
            y += -Input.GetAxis("Mouse Y") * rotateSpeedY * 0.02f;

            Quaternion rotation = Quaternion.Euler(y, x, 0.0f);

            Vector3 position = rotation * new Vector3(0.0f, 0.0f, -offsetDistance) + target.position;

            gameObject.transform.position = position;
            gameObject.transform.rotation = rotation;

        }
        else
        {

            float wantedRotationAngle = target.eulerAngles.y;
            float wantedHeight = target.position.y + offsetHeight;
            float currentRotationAngle = gameObject.transform.eulerAngles.y;
            float currentHeight = gameObject.transform.position.y;

            currentRotationAngle = Mathf.LerpAngle(currentRotationAngle, wantedRotationAngle, rotationDampingX * Time.deltaTime);

            currentHeight = Mathf.Lerp(currentHeight, wantedHeight, rotateSpeedY * Time.deltaTime);

            Quaternion currentRotation = Quaternion.Euler(0f, currentRotationAngle, 0f);

            gameObject.transform.position = target.position;
            gameObject.transform.position -= currentRotation * Vector3.forward * offsetDistance;

            gameObject.transform.position = new Vector3(
                gameObject.transform.position.x,
                currentHeight,
                gameObject.transform.position.z
            );

            gameObject.transform.LookAt(target);

            x = gameObject.transform.position.x;
            y = gameObject.transform.position.y;

        }

    }

}
