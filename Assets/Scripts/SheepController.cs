﻿using UnityEngine;
using UnityEngine.AI;

public class SheepController : MonoBehaviour {

    public Transform target;

    [AutoLoadComponent]
    private NavMeshAgent agent;

    void Awake() {

        agent = gameObject.GetComponent<NavMeshAgent>();

    }

    void Update() {

        if (target != null) {

            agent.SetDestination(target.position);

        }

    }

}
