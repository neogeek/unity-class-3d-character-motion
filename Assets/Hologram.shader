﻿Shader "Unlit/Hologram"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_ColorTint ("ColorTint", Color) = (1, 1, 1, 1)
		_Transparency ("Transparency", Range(0.0, 0.5)) = 0.25
		_CutoutThreshold ("Cutout Threshold", Range(0.0, 1.0)) = 0.2
		_Distance ("Distance", float) = 0.14
		_Amplitude ("Amplitude", float) = 10
		_Amount ("Amount", float) = 0.64
		_Speed ("Speed", float) = 5
	}
	SubShader
	{
		// Queue determins order in which pass is rendered. Transparent needs to be last.
		Tags { "Queue"="Transparent" "RenderType"="Opaque" }
		LOD 100

		// Transparent object do not use the Z Buffer
		ZWrite Off

		// Need to blend our alpha so turn on and subtract to get transparency
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _ColorTint;
			float _Transparency;
			float _CutoutThreshold;
			float _Distance;
			float _Amplitude;
			float _Amount;
			float _Speed;

			v2f vert (appdata v)
			{
				v2f o;
				v.vertex.x += sin(_Time.y * _Speed + v.vertex.y * _Amplitude) * _Distance * _Amount;
				v.vertex.z += sin(_Time.y * _Speed + v.vertex.y * _Amplitude) * _Distance * _Amount;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv) + _ColorTint;
				col.a = _Transparency;

				// if (col.r < _CutoutThreshold) discard;
				clip(col.r - _CutoutThreshold);

				return col;
			}
			ENDCG
		}
	}
}
