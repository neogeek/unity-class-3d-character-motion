﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class MenuControls : MonoBehaviour
{

    public Animator playButton;
    public Animator settingsButton;
    public Animator settingsBox;
    public Animator popUp;
    public Animator gear;

    public Slider masterVolume;
    public AudioMixer mixer;

    private void Awake()
    {

        float value = 0f;

        mixer.GetFloat("MasterVolume", out value);

        masterVolume.value = value;

    }

    public void SettingsToggle(bool isHidden)
    {

        playButton.SetBool("isHidden", isHidden);
        settingsButton.SetBool("isHidden", isHidden);
        settingsBox.SetBool("isHidden", isHidden);

    }

    public void OnMuteChange(bool muted)
    {

        masterVolume.value = muted ? masterVolume.minValue : masterVolume.maxValue;

        mixer.SetFloat("MasterVolume", masterVolume.value);

    }

    public void OnSliderChange()
    {

        mixer.SetFloat("MasterVolume", masterVolume.value);

    }

    public void PopupToggle()
    {

        popUp.SetBool("isHidden", !popUp.GetBool("isHidden"));
        gear.SetBool("isHidden", !gear.GetBool("isHidden"));

    }

}
