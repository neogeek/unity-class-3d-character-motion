﻿using UnityEngine;

public class DoorController : MonoBehaviour
{

    public Vector3 closedPosition;
    public Vector3 openPosiiton;

    public float speed = 0;

    public Vector3 currentPosition;

    void Awake()
    {

        currentPosition = closedPosition;

    }

    void Update()
    {

        gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, currentPosition, Time.deltaTime * speed);

    }

}
