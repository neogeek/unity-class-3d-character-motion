﻿using HutongGames.PlayMaker;
using UnityEngine;
using UnityEngine.UI;

public class UpdateScore : MonoBehaviour
{

    public PlayMakerFSM levelMaster;
    public float PI = 3.14f;

    private Text text;

    void Awake()
    {

        text = gameObject.GetComponent<Text>();

    }

    void Update()
    {

    }

    public void Scored(int points)
    {

        text.text = points.ToString();

        levelMaster.SendEvent("Reset");
        levelMaster.FsmVariables.GetFsmFloat("PI").Value = PI;

    }

}
