﻿using UnityEngine;

public class MiniMapController : MonoBehaviour
{

    public Transform target;

    private Vector3 cameraOffset;

    void Awake()
    {

        cameraOffset = gameObject.transform.position;

    }

    void Update()
    {

        gameObject.transform.position = new Vector3(target.position.x, cameraOffset.y, target.position.z);

    }

}
