﻿Shader "Unlit/AlphaCutout"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_AlphaMask ("Alpha Mask", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Transparent" }
		ZWrite Off
		Lighting Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			SetTexture [_AlphaMask] {combine texture}
			SetTexture [_MainTex] {combine texture, previous}
		}
	}
}
